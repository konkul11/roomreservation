﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoomReservation.Models
{
    public class CalendarItem
    {
        public string Title { get; set; }

        public DateTime Date { get; set; }
    }
}